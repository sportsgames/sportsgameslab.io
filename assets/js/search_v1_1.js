doSearch= function() {
    const val= $("#search-input").val().trim().replace(/ /g, "-");
    const valm= $("#search-input-m").val().trim().replace(/ /g, "-");
    if (val!= "") {
        // alert(val);
        window.location= "/search.html?q="+ val;
    } else if (valm!= "") {
        // alert(val);
        window.location= "/search.html?q="+ valm;
    }
}

$("#search-input").on('keyup', function (e) {
    if (e.key === 'Enter' || e.keyCode === 13) {        
        doSearch();
    }
});

$("#search-input-m").on('keyup', function (e) {
    if (e.key === 'Enter' || e.keyCode === 13) {        
        doSearch();
    }
});

$( "#search-button" ).on( "click", function() {  
    doSearch();
});

$( "#search-button-m" ).on( "click", function() {  
    doSearch();
});

$( document ).ready(function() {
    $("#search-input").focus();
    $("#search-input-m").focus();
});