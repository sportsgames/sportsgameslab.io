document.addEventListener('DOMContentLoaded', function () {
    var toggleButton = document.querySelector('.toggle_button');
    var closeButton = document.querySelector('.close');
    var sidebar = document.querySelector('.sidebar');
    var siteNav = document.getElementById('site-nav');
    var blackScreen = document.querySelector('.black-screen');

    // Toggle sidebar visibility
    toggleButton.addEventListener('click', function () {
        sidebar.classList.toggle('collapse');
        blackScreen.style.display = 'block';
    });

    // Close sidebar
    closeButton.addEventListener('click', function () {
        sidebar.classList.remove('collapse');
        blackScreen.style.display = 'none';
    });

    // Hide sidebar and black screen when clicking on black screen
    blackScreen.addEventListener('click', function () {
        sidebar.classList.remove('collapse');
        blackScreen.style.display = 'none';
    });

    // Show or hide black screen on nav hover for screens wider than 991px
    siteNav.addEventListener('mouseenter', function () {
        if (window.innerWidth >= 991) {
            blackScreen.style.display = 'block';
        }
    });

    siteNav.addEventListener('mouseleave', function () {
        if (window.innerWidth >= 991) {
            blackScreen.style.display = 'none';
        }
    });
});


function recomright(sectionIndex) {
    var gamesSections = document.getElementsByClassName('game-section');
    gamesSections[sectionIndex].scrollBy({
        left: 800,
        behavior: 'smooth'
    });
}

function recomleft(sectionIndex) {
    var gamesSections = document.getElementsByClassName('game-section');
    gamesSections[sectionIndex].scrollBy({
        left: -800,
        behavior: 'smooth'
    });
}


